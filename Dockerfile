FROM centos:7
MAINTAINER The CentOS Project <cloud-ops@centos.org>
LABEL Vendor="CentOS" \
      License=GPLv2 \
      Version=2.4.6-40 \
      description="vortice"

RUN yum -y --setopt=tsflags=nodocs install https://repo.ius.io/ius-release-el7.rpm && \
    yum -y --setopt=tsflags=nodocs update && \
    yum -y --setopt=tsflags=nodocs install python36 && \
    yum -y --setopt=tsflags=nodocs install httpd && \
    yum -y --setopt=tsflags=nodocs install python36-mod_wsgi.x86_64 && \
    yum clean all

COPY requirements.txt /opt/app/requirements.txt
WORKDIR /opt/app
RUN pip3 install -r requirements.txt

WORKDIR /var/www/html/FlaskApp/FlaskApp
COPY FlaskApp.wsgi /var/www/html/FlaskApp/FlaskApp.wsgi
COPY FlaskApp.conf /etc/httpd/conf.d/FlaskApp.conf
COPY __init__.py /var/www/html/FlaskApp/FlaskApp/__init__.py
COPY templates/* /var/www/html/FlaskApp/FlaskApp/templates/
COPY database.db /var/www/html/FlaskApp/FlaskApp/database.db
#Sin los dos RUN siguientes la aplicacion falla al escribir database.db una vez enviado el post en /register
RUN chown -R :apache /var/www/html/FlaskApp/FlaskApp
RUN chmod -R g+w /var/www/html/FlaskApp/FlaskApp

EXPOSE 80

CMD ["/usr/sbin/apachectl","-D","FOREGROUND"]

