## Trabajo Practico Final Actualizacion Tecnologica - ISTEA

<br />
<br />

1. Dockerfile. 

Se desarrollo una aplicacion simple donde un usuario puede registrarse, loguearse y desloguearse.

Dicha imagen se encuentra en el repositorio de Docker Hub.

Para correr la aplicacion SIN CLONAR el proyecto, es decir, 'pulleando' la imagen del repositorio publico:
```
docker run -dit --name flaskLogin -p 8084:80 diegobernalistea/tpfinal
```

<br />
<br />

2. Kubernetes

Se generaron dos yaml, uno de deployment y otro de service, para un despliegue minimo de la aplicacion desarrollada (disponible en Docker Hub)

Desplegamos la aplicacion
```
microk8s kubectl apply -f deployment-flasklogin.yaml
```

Creamos el servicio que nos permitiara acceder via web a nuestra aplicacion utilizando servicio tipo NodePort (por default relaciona el puerto de la aplicacion con un puerto del host en rango 30000-32000)
```
microk8s kubectl apply -f service-flasklogin.yaml
```

Verificamos el puerto asignado en el servicio 
```
microk8s kubectl get svc
```

Finalmente utilizamos dicho puerto con la ip del host para ingresar a la aplicacion.

<br />

---

<br />

Notas:

<br />


